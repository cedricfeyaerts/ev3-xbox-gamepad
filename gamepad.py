from pybricks.tools import print
import struct
import uselect


class Gamepad:

    MAX_STICK_VALUE = 65535
    EVENT_FORMAT = 'llHHl'
    EVENT_SIZE = 16  # struct.calcsize(Gamepad.EVENT_FORMAT)
    INPUT_PATH = "/dev/input/event2"

    def __init__(self):
        self.init_poll()
        self.reset()

    def init_poll(self):
        self.input_file = open(Gamepad.INPUT_PATH, "rb")
        self.event_selector = uselect.poll()
        self.event_selector.register(self.input_file, uselect.POLLIN)

    def reset(self):
        self.a = False
        self.b = False
        self.x = False
        self.y = False
        self.rb = False
        self.lb = False
        self.left_trigger = 0
        self.right_trigger = 0
        self.left_stick_x = 0
        self.left_stick_Y = 0
        self.right_stick_x = 0
        self.right_stick_y = 0
        self.reset_events()

    def reset_events(self):
        self.a_pressed = False
        self.b_pressed = False
        self.x_pressed = False
        self.y_pressed = False

    def read_all_event(self):
        events = self.event_selector.poll(0)
        while len(events) > 0 and events[0][1] and uselect.POLLIN:
            self.read_event(self.input_file)

    def read_event(self):
        """
        Reads events and processes them.
        """

        # reset events
        self.reset_events()

        # Read from the gamepad device virtual file
        event = self.input_file.read(Gamepad.EVENT_SIZE)

        (tv_sec, tv_usec, ev_type, code, value) = struct.unpack(
            Gamepad.EVENT_FORMAT, event)

        if ev_type != 3 and ev_type != 1:
            return

        if ev_type == 3 and code == 0:  # Left Stick Horz. Axis
            self.left_stick_x = self.transform_stick(value)
        elif ev_type == 3 and code == 1:  # Left Stick Vert. Axis
            self.left_stick_y = self.transform_stick(value)
        if ev_type == 3 and code == 2:  # Right Stick Horz. Axis
            self.right_stick_x = self.transform_stick(value)
        elif ev_type == 3 and code == 5:  # Right Stick Vert. Axis
            self.right_stick_y = self.transform_stick(value)
        elif ev_type == 3 and code == 9:  # Xbox Right Trigger
            self.right_trigger = value/1023
        elif ev_type == 3 and code == 10:  # Xbox Left Trigger
            self.left_trigger = value/1023
        elif ev_type == 1 and code == 304:  # A pressed
            if self.a == False and value == 1:
                self.a_pressed = True
            self.a = value == 1
        elif ev_type == 1 and code == 305:  # B pressed
            if self.b == False and value == 1:
                self.b_pressed = True
            self.b = value == 1
        elif ev_type == 1 and code == 307:  # X pressed
            if self.x == False and value == 1:
                self.x_pressed = True
            self.x = value == 1
        elif ev_type == 1 and code == 308:  # Y pressed
            if self.y == False and value == 1:
                self.y_pressed = True
            self.y = value == 1

    def unpack_event(self, device_file):
        """
        Reads events from the gamepad device virtual file
        """
        # Read from the gamepad device virtual file
        event = device_file.read(Gamepad.EVENT_SIZE)

        (tv_sec, tv_usec, ev_type, code, value) = struct.unpack(
            Gamepad.EVENT_FORMAT, event)

        return (tv_sec, tv_usec, ev_type, code, value)

    def transform_stick(self, value):
        """
        Transforms range 0..max to -100..100, removes deadzone from the range.
        """
        max = Gamepad.MAX_STICK_VALUE
        middle = int((max + 1) / 2)
        deadzone = int((max + 1) / 100 * 5)
        value -= middle
        if abs(value) < deadzone:
            value = 0
        elif value > 0:
            value = (value - deadzone - 1) / \
                (middle - deadzone)
        else:
            value = (value + deadzone) / (middle - deadzone)
        return value
