#!/usr/bin/env pybricks-micropython

from pybricks.tools import print
from pybricks import ev3brick as brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import (Port, Stop, Direction, Button, Color,
                                 SoundFile, ImageFile, Align)
from pybricks.tools import print, wait, StopWatch
from pybricks.robotics import DriveBase

import time
import struct
import sys
import uselect
import math


# Write your program here
brick.sound.beep()
right_motor = Motor(Port.B, Direction.COUNTERCLOCKWISE)
left_motor = Motor(Port.C, Direction.COUNTERCLOCKWISE)
gamepad_event_format = 'llHHl'
gamepad_event_size = struct.calcsize(gamepad_event_format)
gamepad_infile = open("/dev/input/event2", "rb")
event_selector = uselect.poll()
event_selector.register(gamepad_infile, uselect.POLLIN)


def process_gamepad_event(device_file):
    """
    Reads events from the gamepad device virtual file and processes them.
    """
    # Read from the gamepad device virtual file
    event = device_file.read(gamepad_event_size)

    (tv_sec, tv_usec, ev_type, code, value) = struct.unpack(
        gamepad_event_format, event)

    if ev_type == 3 or ev_type == 1:

        x_left_value = 0
        y_left_value = 0

        if ev_type == 3 and code == 0:  # Left Stick Horz. Axis
            x_left_value = transform_stick(value)

        elif ev_type == 3 and code == 1:  # Left Stick Vert. Axis
            y_left_value = transform_stick(value)
            # left_motor.run(-8*y_left_value)

        if ev_type == 3 and code == 2:  # Right Stick Horz. Axis
            x_right_value = transform_stick(value)

        elif ev_type == 3 and code == 5:  # Right Stick Vert. Axis
            y_right_value = transform_stick(value)
            # right_motor.run(-8*y_right_value)
            # print(y_right_value)

        # left = max()
        # right_motor.run(8*x_value - )
        # left_motor.run(8*(x_value - y_value))

        elif ev_type == 3 and code == 9:  # Xbox Right Trigger
            # print('right trigger')
            right_motor.run(800*value/1023)

        elif ev_type == 3 and code == 10:  # Xbox Left Trigger
            left_motor.run(800*value/1023)

            # drive(None, value / 1024, None)

            # elif ev_type == 1 and code == 311 and value == 1: # RB pressed
            #     switch_gear(min(gear + 1, 4))
            #     select_gearbox_mode(gearbox_manual)

            # elif ev_type == 1 and code == 310 and value == 1: # LB pressed
            #     switch_gear(max(gear - 1, 1))
            #     select_gearbox_mode(gearbox_manual)

        elif ev_type == 1 and code == 304 and value == 1:  # A pressed
            brick.sound.beep()

            # elif ev_type == 1 and code == 305 and value == 1: # B pressed
            #     select_motors(2 if motors == 1 else 1)

            # elif ev_type == 1 and code == 307 and value == 1: # X pressed
            #     play_horn()

            # elif ev_type == 1 and code == 308 and value == 1: # Y pressed
            #     play_sound_effect()

        speeds = drive(x_left_value/100, y_left_value/100)
        # print(speeds)
    # left_motor.run(800*speeds[0])
    # right_motor.run(800*speeds[1])


def transform_stick(value):
    """
    Transforms range 0..max to -100..100, removes deadzone from the range.
    """
    max = 65535
    half = int((max + 1) / 2)
    deadzone = int((max + 1) / 100 * 5)
    value -= half
    if abs(value) < deadzone:
        value = 0
    elif value > 0:
        value = (value - deadzone - 1) / (half - deadzone) * 100
    else:
        value = (value + deadzone) / (half - deadzone) * 100
    return value


def drive(x, y):
    print(x)

    left = 0
    right = 0
    if x == 0:
        left = y
        right = y
    elif y == 0:
        left = y
        right = -y
    elif x > 0 and y > 0:
        right = y - x
        left = magnitude(x, y)
    elif x > 0 and y < 0:
        left = y + x
        right = - magnitude(x, y)
    elif x < 0 and y > 0:
        left = y + x
        right = magnitude(x, y)
    elif x < 0 and y < 0:
        right = y - x
        left = - magnitude(x, y)

    return (left, right)


def magnitude(x, y):
    return math.sqrt(x * x + y * y)


while True:
    events = event_selector.poll(0)
    if (len(events) > 0 and events[0][1] & uselect.POLLIN):
        process_gamepad_event(gamepad_infile)
    else:
        # This code is executed when there are no gamepad events to proceed.
        # Here we can check sensors or do some other work.
        # automatic_gearbox_control()
        # sleep a little to reduce CPU load and power consumption
        time.sleep(0.510)
