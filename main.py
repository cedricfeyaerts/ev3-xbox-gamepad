#!/usr/bin/env pybricks-micropython

from pybricks import ev3brick as brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import (Port, Stop, Direction, Button, Color,
                                 SoundFile, ImageFile, Align)
from pybricks.tools import print, wait, StopWatch
from pybricks.robotics import DriveBase

from gamepad import Gamepad
import time


brick.sound.beep()
right_motor = Motor(Port.B, Direction.COUNTERCLOCKWISE)
left_motor = Motor(Port.C, Direction.COUNTERCLOCKWISE)

gamepad = Gamepad()

while True:
    gamepad.read_event()
    # gamepad.read_all_event()
    # print(gamepad.a, gamepad.a_pressed)
    if (gamepad.a_pressed):
        brick.sound.file(SoundFile.GREEN)
    if (gamepad.b_pressed):
        brick.sound.file(SoundFile.RED)
    if (gamepad.x_pressed):
        brick.sound.file(SoundFile.BLUE)
    if (gamepad.y_pressed):
        brick.sound.file(SoundFile.YELLOW)
    print(gamepad.left_stick_x)
    if (gamepad.left_stick_x > 0):
        right_motor.run(800*gamepad.right_trigger*(1-gamepad.left_stick_x))
        left_motor.run(800*gamepad.right_trigger)
    elif (gamepad.left_stick_x < 0):
        right_motor.run(800*gamepad.right_trigger)
        left_motor.run(800*gamepad.right_trigger*(1+gamepad.left_stick_x))
    else:
        right_motor.run(800*gamepad.right_trigger)
        left_motor.run(800*gamepad.right_trigger)

    # gamepad.reset()
    # time.sleep(0.050)
